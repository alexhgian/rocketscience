GameAssets.png
format: RGBA8888
filter: Linear,Linear
repeat: none
building2
  rotate: false
  xy: 804, 2
  size: 70, 90
  orig: 128, 128
  offset: 27, 16
  index: -1
enemyBullet
  rotate: false
  xy: 926, 38
  size: 31, 44
  orig: 35, 50
  offset: 2, 6
  index: -1
enemyTank
  rotate: false
  xy: 926, 2
  size: 48, 34
  orig: 64, 64
  offset: 9, 8
  index: -1
BG
  rotate: false
  xy: 2, 2
  size: 800, 480
  orig: 800, 480
  offset: 0, 0
  index: -1
myTank
  rotate: false
  xy: 876, 2
  size: 48, 44
  orig: 64, 64
  offset: 9, 15
  index: -1
bullet
  rotate: false
  xy: 976, 2
  size: 31, 44
  orig: 35, 50
  offset: 2, 0
  index: -1
