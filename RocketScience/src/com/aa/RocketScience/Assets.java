package com.aa.RocketScience;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {
	
	
	public static TextureAtlas atlas;
	public static TextureRegion tank;
	public static Texture bfTank;
	public static Texture bullet;
	public static TextureRegion backGround;
	public static TextureRegion building;
	
	//public static TextureRegion bullet;
	
	
	
	

	public static void load() {
		atlas = new TextureAtlas(Gdx.files.internal("GameAssets.txt"));
		tank = atlas.findRegion("myTank");
		backGround = atlas.findRegion("BG");
		building = atlas.findRegion("building2");
		bfTank = new Texture(Gdx.files.internal("data/lavSized2.png"));
		//bullet = atlas.findRegion("bullet");
		bullet = new Texture(Gdx.files.internal("data/TankBullet.png"));
	}

	public static void dispose() {
		atlas.dispose();
	}

}
