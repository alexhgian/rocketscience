package com.aa.RocketScience.entities;

import com.aa.RocketScience.Assets;
import com.aa.RocketScience.audio.Sfx;
import com.aa.RocketScience.weapons.Bullet;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.WheelJoint;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.TimeUtils;


public class Tank {
	private World world;	
	private Sprite tankSprite;
	private Body chassis, leftWheel, rightWheel, midLeftWheel,midRightWheel;
	WheelJoint leftAxis,rightAxis, midLeftAxis, midRightAxis;
	
	public static float speed = 999999;
	//public int moveSpeed = 1000;
	//private Bullet bullet;
	
	public boolean isPlayer;
	public int hitCount = 0;
	private long previousTime=0;
	
	public Tank(World w, FixtureDef chassisFD, FixtureDef wheelFD, float x, float y, float width, float height)
	{
		world = w;
		BodyDef bodyDef = new BodyDef();
		bodyDef.position.set(x,y);
		bodyDef.type = BodyType.DynamicBody;
		
		//chassis 
		PolygonShape chassisShape = new PolygonShape();
		chassisShape.set(new float[] {
				-width/2-12,-height/2,//bottom left				
				width/2+12,-height/2 , //top left			
				width/2-16, height/2, //bottom left
				-width/2-12, height/2
				});//bottom right
		
		chassisFD.shape = chassisShape;
		
	
		chassis = w.createBody(bodyDef);
		chassis.createFixture(chassisFD);
		tankSprite = new Sprite(Assets.bfTank);	
        chassis.setUserData(this);
        
		//left wheel
		CircleShape wheelShape = new CircleShape();
		wheelShape.setRadius(height / 4.5f);
		
		wheelFD.shape = wheelShape;
		//left wheel
		leftWheel = w.createBody(bodyDef);
		leftWheel.createFixture(wheelFD);
		
		//right wheel
		rightWheel = w.createBody(bodyDef);
		rightWheel.createFixture(wheelFD);
		//mid left wheel
		midLeftWheel = w.createBody(bodyDef);
		midLeftWheel.createFixture(wheelFD);
		//mid right wheel
		midRightWheel = w.createBody(bodyDef);
		midRightWheel.createFixture(wheelFD);
		
		//left axis
		WheelJointDef axisDef = new WheelJointDef();
		axisDef.bodyA = chassis;
		axisDef.bodyB = leftWheel;		
		axisDef.frequencyHz = 1f;
		System.err.println("Density: " + chassisFD.density);
		axisDef.maxMotorTorque = chassisFD.density*400000;
		axisDef.localAnchorA.set(-width/2 * .7f, -height/2 * 1.5f);
		axisDef.localAxisA.set(Vector2.Y);//Move freely in the y direction only		
		leftAxis = (WheelJoint) world.createJoint(axisDef);
		
		//right axis
		axisDef.bodyB = rightWheel;
		axisDef.localAnchorA.x *=-1.15;		
		rightAxis = (WheelJoint) world.createJoint(axisDef);
		
		//mid axis
		axisDef.bodyB = midLeftWheel;
		axisDef.localAnchorA.x = -width/2 * .25f;
		midLeftAxis = (WheelJoint) world.createJoint(axisDef);
		
		//mid axis
		axisDef.bodyB = midRightWheel;
		axisDef.localAnchorA.x  *=-1.2;
		midRightAxis = (WheelJoint) world.createJoint(axisDef);
		
	}
	
	public Sprite getSprite()
	{
		return tankSprite;
	}
	public void fire()
	{
		long currentTime = TimeUtils.millis();		
		if (currentTime - previousTime > 500)//.5 seconds
		{						
			Sfx.firing.play();
			float x = chassis.getPosition().x;
			float y = chassis.getPosition().y;
			System.err.println(x);
			Bullet bullet = new Bullet(world);
			bullet.spawn(x+80, y+32, (float)(chassis.getAngle()+ MathUtils.degreesToRadians *0), Assets.bullet);
			
			previousTime = currentTime;
		}		
		
	}
	public void controller(Touchpad tp)
	{
		float xPer = tp.getKnobPercentX();
		float yPer = tp.getKnobPercentY();
		float threshold = .1f;
		
		if(xPer > threshold){
			leftAxis.enableMotor(true);
			leftAxis.setMotorSpeed(-speed);
			rightAxis.enableMotor(true);
			rightAxis.setMotorSpeed(-speed);
			midLeftAxis.enableMotor(true);
			midLeftAxis.setMotorSpeed(-speed);
			midRightAxis.enableMotor(true);
			midRightAxis.setMotorSpeed(-speed);
		} else if (xPer < -threshold){
			leftAxis.enableMotor(true);
			leftAxis.setMotorSpeed(speed);
			rightAxis.enableMotor(true);
			rightAxis.setMotorSpeed(speed);
			midLeftAxis.enableMotor(true);
			midLeftAxis.setMotorSpeed(speed);
			midRightAxis.enableMotor(true);
			midRightAxis.setMotorSpeed(speed);
		} else {
			leftAxis.enableMotor(true);
			leftAxis.setMotorSpeed(0);
			rightAxis.enableMotor(true);
			rightAxis.setMotorSpeed(0);
			midLeftAxis.enableMotor(true);
			midLeftAxis.setMotorSpeed(0);
			midRightAxis.enableMotor(true);
			midRightAxis.setMotorSpeed(0);
		}

		
	}
	
	public Body getChassis()
	{	
		return chassis;
	}	


	
}
