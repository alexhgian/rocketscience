package com.aa.RocketScience.ui;

import java.util.Iterator;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class MyInputProcessor implements InputProcessor {
	private World world;
	private Array<Body> bodies;
	private OrthographicCamera camera;
    Vector3 testPoint = new Vector3();
   // QueryCallback callback = new QueryCallback();
    
	   public MyInputProcessor(){		   
	   }
	   public MyInputProcessor(World myWorld, Array<Body> bodies,OrthographicCamera camera) {		   
		   world = myWorld;
		   this.bodies = bodies;
		   this.camera = camera;
	   }
	   
	   @Override
	   public boolean keyDown (int keycode) {
	      return false;
	   }

	   @Override
	   public boolean keyUp (int keycode) {
	      return false;
	   }

	   @Override
	   public boolean keyTyped (char character) {
	      return false;
	   }

	   @Override
	   public boolean touchDown (int x, int y, int pointer, int button) {
	      return false;
	   }

	   @Override
	   public boolean touchUp (int x, int y, int pointer, int button) {
	      return false;
	   }

	   @Override
	   public boolean touchDragged (int x, int y, int pointer) {
		   camera.unproject(testPoint.set(x, y, 0));
		   
		   System.err.println("touchDragged() X: " + x + " Y: " + y + " Pointer: " + pointer);		 
		   Iterator<Body> bi = bodies.iterator();   
  
		  
           while (bi.hasNext()){
			    Body b = bi.next();
			    b.setTransform(new Vector2(x,720-y), 0);
			   
			    
			}
		   
		   return false;
	   }

	   public boolean touchMoved(int x, int y) {
	      return false;
	   }

	   @Override
	   public boolean scrolled (int amount) {
	      return false;
	   }

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}
	}
