package com.aa.RocketScience.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class Styles {
	private static Skin uiSkin;
	private static BitmapFont white;	
	
	
	public static TextButtonStyle getMenuButton()
	{
		TextureAtlas atlasUI = new TextureAtlas(Gdx.files.internal("ui/uiskin.atlas"));
		uiSkin = new Skin(atlasUI);
		//Create the button style
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = uiSkin.getDrawable("default-round");
		textButtonStyle.down = uiSkin.getDrawable("default-round-down");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;	
		
		white = new BitmapFont();
		white.setScale(1.5f);
		textButtonStyle.font = white;	
		return textButtonStyle;
	}
}
