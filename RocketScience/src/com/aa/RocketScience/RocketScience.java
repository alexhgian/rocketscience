package com.aa.RocketScience;

import com.aa.RocketScience.audio.Bgm;
import com.aa.RocketScience.audio.Sfx;
import com.aa.RocketScience.screens.Play;
import com.badlogic.gdx.Game;

public class RocketScience extends Game {
	//private OrthographicCamera camera;
	//private SpriteBatch batch;
	//private Texture texture;

	
	@Override
	public void create() {		
		Assets.load();
		Bgm.load();
		Sfx.load();
		//float w = Gdx.graphics.getWidth();
		//float h = Gdx.graphics.getHeight();		
   		//camera = new OrthographicCamera();
   		//camera.setToOrtho(false, 800, 480);
        this.setScreen(new Play(this));
	}

	@Override
	public void dispose() {
		//batch.dispose();
		//texture.dispose();
		Bgm.dispose();
		Sfx.firing.dispose();
	}

	@Override
	public void render() {		
       super.render();	

	}

	@Override
	public void resize(int width, int height) {
		//camera.setToOrtho(false, width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
