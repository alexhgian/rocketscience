package com.aa.RocketScience.screens;

import com.aa.RocketScience.Assets;
import com.aa.RocketScience.RocketScience;
import com.aa.RocketScience.audio.Bgm;
import com.aa.RocketScience.entities.PlayerTank;
import com.aa.RocketScience.entities.Tank;
import com.aa.RocketScience.listeners.CollisionDetection;
import com.aa.RocketScience.ui.MyTouchPad;
import com.aa.RocketScience.ui.Styles;
import com.aa.RocketScience.weapons.Bullet;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

public class Play implements Screen  {
	private static final int VIRTUAL_WIDTH = 800;
	private static final int VIRTUAL_HEIGHT = 480;
	private static final float ASPECT_RATIO = (float)VIRTUAL_WIDTH/(float)VIRTUAL_HEIGHT;
	
    //public MainMenuScreen(final Drop gam)....        
    final RocketScience game;
    
    OrthographicCamera camera;
    
    private TextureAtlas atlas;
    private Stage stage;
    private Skin skin;
    private Table table;
    private TextButton buttonFire,musicButton;
    private BitmapFont white,black;
    private Label heading;
    
    private Tank playerTank;
   
    static final float WORLD_TO_BOX = 0.01f;
    static final float BOX_TO_WORLD = 100f;
	private static final float GRAVITY = -10;
    
    private Array<Body> tmpBodies;
    private SpriteBatch batch;
    
    private World world;
    Box2DDebugRenderer debugRenderer;
    
    private long previousTime = 0;
    Sprite playerSprite;
    
    public Touchpad touchpad;
	
    
   	public Play(final RocketScience gam) {
   		game = gam;
    
   		camera = new OrthographicCamera();
   		camera.setToOrtho(false, VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
   		world = new World(new Vector2(0, GRAVITY), true); 
   		//debugRenderer = new Box2DDebugRenderer(false, false, false, false, false, false );  	
   		debugRenderer = new Box2DDebugRenderer(true, true, false, true, true, true );  		
   		batch = new SpriteBatch();		
   		
   		
   		//Set up some bodies in our world
   		//wall(40.0f);
   		//wall(VIRTUAL_WIDTH - 40);
   		platform(40.0f);
   		platform(VIRTUAL_HEIGHT - 40);
   		
   	    //adds collision detection
   		world.setContactListener(new CollisionDetection());
   	}
	@Override
    public void render(float delta) {		
	    Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
	    Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);  
	    camera.position.set(playerTank.getChassis().getPosition().x,
	    					playerTank.getChassis().getPosition().y,0);
	    camera.update();
	    
	    //Tank controlling method
	    
		/*

		*/
        //playerTank.move(touchpad.getKnobPercentX(), touchpad.getKnobPercentY());
	    playerTank.controller(touchpad);
	   
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		tmpBodies = new Array<Body>();
		world.getBodies(tmpBodies);
		for(Body body: tmpBodies)
		{
			if(body.getUserData() != null && body.getUserData() instanceof Bullet) 
			{
				Bullet bullet = (Bullet) body.getUserData();
				Sprite sprite = bullet.getSprite();
				sprite.setPosition(body.getPosition().x-sprite.getWidth()/2, body.getPosition().y-sprite.getHeight()/2);
				sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
				sprite.draw(batch);
			}
			else if (body.getUserData() != null && body.getUserData() instanceof Tank)
			{
				Tank Tank = (Tank) body.getUserData();
				Sprite sprite = Tank.getSprite();
				sprite.setPosition(body.getPosition().x-sprite.getWidth()/2, body.getPosition().y-sprite.getHeight()/2+10);
				sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
				sprite.draw(batch);
			}
			//TODO make this work for tanks as well
			//Deletion check for bullets
			if( body != null && body.getUserData() instanceof Bullet)
			{
				Bullet data = (Bullet) body.getUserData();
	            if(data.isFlaggedForDelete) {
	            world.destroyBody(body);
	            	body.setUserData(null);
	            	body = null;
	            }
	        }
		}	
		batch.end();
		
		debugRenderer.render(world, camera.combined); 
		world.step(1/60f, 6, 2);
		stage.act(delta);
		Table.drawDebug(stage);
		stage.draw();  
    }
	public void wall(float x)
	{
		// Create our body definition
		BodyDef groundBodyDef =new BodyDef();  
		// Set its world position
		groundBodyDef.position.set(new Vector2(x, 720/2));  

		// Create a body from the defintion and add it to the world
		Body groundBody = world.createBody(groundBodyDef);  

		// Create a polygon shape
		PolygonShape groundBox = new PolygonShape();  
		// Set the polygon shape as a box which is twice the size of our view port and 20 high
		// (setAsBox takes half-width and half-height as arguments)
		groundBox.setAsBox(10, camera.viewportHeight);
		// Create a fixture from our polygon shape and add it to our ground body  
		groundBody.createFixture(groundBox, 0.0f); 
		// Clean up after ourselves
		groundBox.dispose();
	}	
	
	public void platform(float y){
		// Create our body definition
		BodyDef groundBodyDef =new BodyDef();  
		// Set its world position
		groundBodyDef.position.set(new Vector2(1280/2, y));  

		// Create a body from the defintion and add it to the world
		Body groundBody = world.createBody(groundBodyDef);  

		// Create a polygon shape
		PolygonShape groundBox = new PolygonShape();  
		// Set the polygon shape as a box which is twice the size of our view port and 20 high
		// (setAsBox takes half-width and half-height as arguments)
		groundBox.setAsBox(camera.viewportWidth, 10);
		// Create a fixture from our polygon shape and add it to our ground body  
		groundBody.createFixture(groundBox, 0.0f); 
		// Clean up after ourselves
		groundBox.dispose();
	}

	@Override
	public void resize(int width, int height) {
		System.err.println("MainMenuScreen.java Resize - Width: " + width + ", Height: " + height);
		//TODO Auto-generated method stub
		//camera.setToOrtho(false, width, height);
		stage.setViewport(width, height, true);
		//table.setClip(true);
		
		
		//table.setBounds(0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
	}

	@Override
	public void show() {
		Bgm.play();
		spawnPlayer();
		
		System.err.println("In method show()");
		stage = new Stage();			
		
		InputMultiplexer multiplexer = new InputMultiplexer();
		//multiplexer.addProcessor(new MyInputProcessor(world,bodies,camera));
		multiplexer.addProcessor(stage);		
		Gdx.input.setInputProcessor(multiplexer);		
		
		table = new Table();   
		table.setBounds(0,0,VIRTUAL_WIDTH,VIRTUAL_HEIGHT/4);
		table.setClip(true);
		
		buttonFire = new TextButton("Fire", Styles.getMenuButton());
		buttonFire.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				playerTank.fire();
				//Bullet bullet = new Bullet(world);
				//bullet.spawn(Assets.bullet);//Give it any sprite to link to box2d body
			}			
		});
        int buttonSize = 64*Gdx.graphics.getHeight()/VIRTUAL_HEIGHT;
		buttonFire.setBounds(Gdx.graphics.getWidth()-buttonSize-10, 10, buttonSize, buttonSize);
		musicButton = new TextButton("Music", Styles.getMenuButton());
		musicButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				
				if( Bgm.music.isPlaying())
				{
					Bgm.pause();
				} else {
					Bgm.play();
				}
			}			
		});
		
		musicButton.setBounds(Gdx.graphics.getWidth()-buttonSize/2-10, Gdx.graphics.getHeight()-buttonSize/2-10, buttonSize/2, buttonSize/2);
		
		touchpad = (new MyTouchPad(Gdx.graphics.getWidth(),Gdx.graphics.getWidth())).getTouchPad();
		
		//table.add(touchpad);		
		//table.add(buttonFire);//Add button to table
		stage.addActor(musicButton);
		stage.addActor(buttonFire);
		stage.addActor(touchpad);//Add table to stage
		

	}
	public void spawnPlayer()
	{   		
   		FixtureDef fixtureDef = new FixtureDef();
   		FixtureDef wheelFD = new FixtureDef();
   		
   		fixtureDef.density = 1f;
   		fixtureDef.friction = .4f;
   		fixtureDef.restitution = 1f;
   		
   		
   		wheelFD.density = 1f;
   		wheelFD.friction = 20f;
   		wheelFD.restitution = 1f;
   		
   		
   	    playerTank = new PlayerTank(world, fixtureDef, wheelFD, 150, 100, 150, 40f);
        new Tank(world, fixtureDef, wheelFD, 0, 150, 150, 45f);
        new Tank(world, fixtureDef, wheelFD, 800, 100, 150, 45f);
        new Tank(world, fixtureDef, wheelFD, 1000, 100, 150, 45f);
        
        //playerTank.spawn(Assets.tank);	
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		skin.dispose();
		atlas.dispose();
		white.dispose();
		black.dispose();
		playerSprite.getTexture().dispose();
	}



}