package com.aa.RocketScience.listeners;

import com.aa.RocketScience.entities.Tank;
import com.aa.RocketScience.weapons.Bullet;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class CollisionDetection implements ContactListener{

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		//System.err.println("Collision");
	    Body a = contact.getFixtureA().getBody();
	    Body b = contact.getFixtureB().getBody();
	    Object objA = a.getUserData();
	    Object objB = b.getUserData();
	    /*
	    if( objA instanceof Bullet)
	    	System.err.println("ObjA is Bullet");
	    if( objA instanceof Tank)
	    	System.err.println("ObjA is Tank");
	    if( objB instanceof Bullet)
	    	System.err.println("ObjB is Bullet");
	    if( objB instanceof Tank)
	    	System.err.println("ObjB is Tank");
	   */
	    
	    if(objA instanceof Tank && objB instanceof Bullet)
	    {
	    	System.err.println("Tank to Bullet Collision");
	
	    	 ((Tank)objA).hitCount++;
	    	 int count = ((Tank)objA).hitCount;
	    	 System.err.println("Hits: " + count);
	    	 
	    	((Bullet)objB).isFlaggedForDelete = true;
	    	
	
	    }
		
		
	}
	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
