package com.aa.RocketScience.weapons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Bullet {

	private World world;
	private Sprite bulletSprite;
	public boolean isFlaggedForDelete = false; 
	public Bullet(World w)
	{
		world = w;
	}
	public Sprite getSprite()
	{
		return bulletSprite;
	}
	public void spawn(float x, float y, float angle, Texture texture){
		// First we create a body definition
		BodyDef bodyDef = new BodyDef();
		// We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
		bodyDef.type = BodyType.DynamicBody;
		// Set our body's starting position in the world
		bodyDef.position.set(x, y);
		bodyDef.angle = angle;
		// Create our body in the world using our body definition
		Body body = world.createBody(bodyDef);
		body.setLinearVelocity(100,10);
		
		//body.setAngularVelocity(.1f);
        bulletSprite = new Sprite(texture);
        body.setUserData(this);

		System.err.println(world.getBodyCount());
	
		// Create a circle shape and set its radius to 6
		CircleShape circle = new CircleShape();
		circle.setRadius(6f);

		// Create a fixture definition to apply our shape to
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.density =  0.1f;//0.5f; 
		fixtureDef.friction = 1f;
		fixtureDef.restitution = 0.1f; // Make it bounce a little bit
       
		// Create our fixture and attach it to the body
		//Fixture fixture = body.createFixture(fixtureDef);
		body.createFixture(fixtureDef);
		// Remember to dispose of any shapes after you're done with them!
		// BodyDef and FixtureDef don't need disposing, but shapes do.
		circle.dispose();
	}
}
