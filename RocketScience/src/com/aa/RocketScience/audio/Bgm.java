package com.aa.RocketScience.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class Bgm {
	public static Music music;
	public static void load()
	{
		    music = Gdx.audio.newMusic(Gdx.files.internal("audio/RittaiKidou.mp3"));
			music.setVolume(1f); 
			music.setLooping(true);
	}
	public static void play()
	{	
		music.play();   
	}
	public static void pause()
	{
		music.pause();
	}
	public static void dispose()
	{
		music.dispose();
	}
}
